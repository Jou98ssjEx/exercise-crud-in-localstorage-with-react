import axios from 'axios'

const urlGetPokemon = 'https://pokeapi.co/api/v2';
// const urlGetPokemonById = '';

export const getPokemonApi = axios.create({
    baseURL: urlGetPokemon,
});

export const getPokemonApiById = axios.create({
    baseURL: `${urlGetPokemon}/pokemon/`,
})