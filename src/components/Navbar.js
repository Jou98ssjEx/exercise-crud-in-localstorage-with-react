import React, { useContext } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { RemoveMsgError } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext';
import { routes } from '../routes/routerLink'

export const Navbar = () => {
  const { dispatch } = useContext(AuthContext);

  const handleResetMsg = ( ) => {
    dispatch(RemoveMsgError());
  }
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <i className=' navbar-brand bx bxs-notepad'> Crud App </i>

            {/* <a className="navbar-brand" href="#">Crud App</a> */}
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                {
                  routes.map( rute => (
                    <li
                      className='nav-item'
                      key={ rute.name }
                    >
                      <NavLink 
                        to={ rute.route }
                        className='nav-link'
                      >
                        { rute.name }
                      </NavLink>

                    </li>
                  ))
                }
                  
                  {/* <li className="nav-item">
                    <NavLink
                      to='/'
                      className='nav-link'
                    >
                      Home
                    </NavLink>
                  </li>
                  <li className="nav-item">
                  <NavLink
                      to='/pokemons'
                      className='nav-link'
                    >
                      Pokemons
                    </NavLink>
                  </li> */}
                
              </ul>
              <form className="d-flex">
                  {/* <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/> */}
                  <button onClick={ handleResetMsg } className="btn btn-outline-success" type="button">ResetError</button>
              </form>
            </div>
        </div>
    </nav>
  )
}
