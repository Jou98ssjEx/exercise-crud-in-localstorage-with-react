import { types } from "../types/types"
import Swal from "sweetalert2"
import { useContext } from "react";
import { AuthContext } from "../auth/AuthContext";

// Crud actions

export const AddPokemon = ( data ) => {


    Swal.fire({
        title: `Pokemon ${data.name} save in LocalStorage`,
        imageUrl: data.img,
        imageWidth: 150,
        imageHeight: 150,
        imageAlt: 'Custom image',
        icon: 'success',
        showConfirmButton: false,
        timer: 1500,
    })
    
    return {
        type: types.Add,
        payload: data,
    }
}
// export const AddPokemon = ( data ) => ({
//     type: types.Add,
//     payload: data,
// })

export const DeletePokemon = ( idPokemon ) => ({
    type: types.Delete,
    payload: idPokemon, 
})

export const ActualPokemon = ( dataPokemon ) => ({
    type: types.Actual,
    payload: dataPokemon,
})

export const UpdatePokemon = ( dataPokemon ) => ({
    type: types.Update,
    payload: dataPokemon
})

// Msg Errors

export const SetMsgError = ( msgError ) =>({
    type: types.MsgError,
    payload: msgError,
})

export const RemoveMsgError = () => ({
    type: types.RemoveMsgError,
})