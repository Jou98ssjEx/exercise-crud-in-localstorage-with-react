import React, { useContext, useState } from 'react'
import { RemoveMsgError, SetMsgError } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext';

export const SearchPokemon = ({ setPokemonIdOrName }) => {
    const { dispatch } = useContext(AuthContext);

    // setPokemonIdOrName('');

    const [inputValue, setInputValue] = useState('');

    const inputChange = ( {target} ) => {
        setInputValue(target.value);
    }

    const handleSumit = ( e ) => {
        e.preventDefault();

        // if(!isInputValid()){
        // }
        isInputValid();
        setPokemonIdOrName(inputValue);
        setInputValue('');
    }

    const isInputValid = ( ) =>{

        if(inputValue === ''){
            dispatch(SetMsgError(`Id or Name is required`))
            return false;
        }
        dispatch(RemoveMsgError());
        return true;
    }

  return (
    <div className='col-12 col-md-6'>
        <div className='card'>
            <form
                className=' mb-3 p-4'
                onSubmit={ handleSumit }
            >
                <label
                    className='form-input mb-3'
                >
                    Name or Id:
                </label>
                <input
                    name='value'
                    type='text'
                    onChange={ inputChange }
                    value={inputValue}
                    // autoFocus='on'
                    autoComplete='off'
                    className='form-control mb-3'
                />

                <input 
                    type='button'
                    className='btn btn-outline-success w-100'
                    value={`Enviar`}
                    onClick={ handleSumit }
                />
            </form>
        </div>
    </div>
  )
}
