import { useState } from "react";
const xPokemon = 12;
export const useFilteredPokemon = ( pokemons ) => {

    const [ currentPage, setCurrentPage ] = useState(0)
    const [ search, setSearch ] = useState('');

    const fiteredPokemons = ( ) => {
    
        if( search.length === 0 ) 
            return pokemons.slice(currentPage, currentPage + xPokemon);

        // Si hay algo en la caja de texto
        const filtered = pokemons.filter( poke => poke.name.includes( search ) );
        return filtered.slice( currentPage, currentPage + xPokemon);
    }

    const next = () => {

        if ( pokemons.filter( poke => poke.name.includes( search ) ).length > currentPage + xPokemon )
            setCurrentPage( currentPage + xPokemon );

    }

    const previous = () => {
        if ( currentPage > 0 )
            setCurrentPage( currentPage - xPokemon );

    }

    const onSearchChange = ( { target }) => {
        setCurrentPage(0);
        setSearch( target.value );
    }

    return [ fiteredPokemons, next, previous, currentPage, {search, onSearchChange} ];

}