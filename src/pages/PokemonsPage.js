import React from 'react'
import { Loading } from '../components/Loading';
import { PokemonCard } from '../components/PokemonCard';
import { useFilteredPokemon } from '../hooks/useFilteredPokemon';
import { usePokemons } from '../hooks/usePokemons'

export const PokemonsPage = () => {

  const { pokemons, loading } = usePokemons();
  // console.log(pokemons, loading)

  // pagination more fitered of pokemons
  const [ fiteredPokemons, next, previous, currentPage, { search, onSearchChange }] = useFilteredPokemon(pokemons);

  return (
    <div className='container mt-2'>
      <div className='row'>
        <div className='col-12 d-flex justify-content-between'>
          <h2>All Pokemons</h2>
          <input 
              type='text'
              className="mb-2 form-control searching"
              placeholder="Searching Pokémon"
              value={ search }
              onChange={ onSearchChange }
          />
        </div>
        <hr/>
      </div>

      <div className='row'>
        <div className='col d-flex justify-content-around '>
          {/* <div>
            <input 
              type='text'
            />
          </div> */}
            <input 
              type='button'
              value={`Previous`}
              onClick={ previous }
              disabled={(!currentPage >0) ? true: false}
              className={`pointer btn btn-outline-info`}
              />
            <input 
              type='button'
              value={`Next`}
              onClick={ next }
              disabled={(currentPage >0) ? true: false}
              className={`pointer btn btn-outline-info`}


            />
          {/* <div className='d-flex justify-content-between'>
          </div> */}
        </div>  
      </div>

      <div className='row mt-3'>
        {
          (loading)
            ? (
              <Loading />
            )
            : ( fiteredPokemons().length === 0 )
              ? <div> <h2>Pokemons don´t exits</h2> </div>
              
            : (
              fiteredPokemons().map( poke => (
                <PokemonCard
                  key={poke.id}
                  pokemon= { poke }
                />
              ))
            )
        }
      </div>
    </div>
  )
}
