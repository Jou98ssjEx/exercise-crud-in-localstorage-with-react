import { useEffect, useState } from "react"
import { getPokemonsOfApi, getPokemon } from "../helpers/getPokemons"

export const usePokemons = ( ) => {

    const [data, setData] = useState({
        pokemons: [],
        loading: true,
    })

    useEffect(() => {
        getPokemonsOfApi()
        .then(
            pokemons =>{
                setData({
                    pokemons,
                    loading: false
                })
            }
        )
        // .catch(console.log('Error in usePokemons'))
    }, [])

    return data;
    
}

export const usePokemon = ( idOrName = '1' ) => {

    const [data, setData] = useState({
        pokemon: {},
        loading: true,
        msgError: null,
    });

    useEffect(() => {
        getPokemon( idOrName )
            .then(pokemon =>{
                setData({
                    pokemon,
                    loading: false,
                });
            })
            // .catch(console.log )
            .catch(setData({
                msgError: true
            }))
      
    }, [ idOrName ]);

    return data;

}