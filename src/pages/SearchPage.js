import React, { useContext, useEffect, useState } from 'react'
import { RemoveMsgError, SetMsgError } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext';
import { Loading } from '../components/Loading';
import { MsgError } from '../components/MsgError';
import { SearchPokemon } from '../components/SearchPokemon'
import { ShowPokemon } from '../components/ShowPokemon'

export const SearchPage = () => {
    const { state } = useContext(AuthContext);
    const { msgError } = state;
     // dispatch(RemoveMsgError());


    const [pokemonIdOrName, setPokemonIdOrName] = useState('');

    // useEffect(() => {
    //   isInputValid();
    // //   dispatch(RemoveMsgError());

    // }, [pokemonIdOrName])
    
    // const isInputValid = ( ) =>{

    //     if(pokemonIdOrName === ''){
    //         dispatch(SetMsgError(`Id or Name is required`))
    //         return false;
    //     }
    //     dispatch(RemoveMsgError());
    //     return true;
    // }
    
  return (
    <div className='container mt-2'>
        
        <div className='row'>
            <div className='col-12'>
                <h3>Search Pokemon for name or id</h3>
                <hr/>
            </div>
            {
                (msgError !== null) && <MsgError />
            }

            <SearchPokemon setPokemonIdOrName={ setPokemonIdOrName } />

            {
                // (msgError !== null) ?
                //     <ShowPokemon pokemonIdOrName={ pokemonIdOrName } />
                //     :
                //     <MsgError />
                // (pokemonIdOrName !== '') ?
                //     <ShowPokemon pokemonIdOrName={ pokemonIdOrName } />
                //     :
                //     <MsgError />
               
                (pokemonIdOrName !== '') &&
                    <ShowPokemon pokemonIdOrName={ pokemonIdOrName } />
                   
            }

        </div>
    </div>
  )
}
