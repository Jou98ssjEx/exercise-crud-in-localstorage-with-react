import { types } from "../types/types";

const initialState = {
    auth: {
        user: 'Barry Allen',
        email: 'flash98@labstart.com',
    },
    dataList: [],
    actualData: {},
    msgError: null,
}

export const crudReducer = ( state = initialState, action) => {

    switch (action.type) {
        case types.Add:

            return {
                ...state,
                dataList: [...state.dataList, action.payload],
            }

        case types.Delete:
            return {
                ...state,
                actualData: {},
                dataList: state.dataList.filter( d  => d.idPokemon !== action.payload),
            }

        case types.Actual:
            
            return {
                ...state,
                actualData: action.payload, 
            }

        case types.Update:
            
            return{
                ...state,
                dataList: state.dataList.map(
                    data =>
                    data.idPokemon === action.payload.idPokemon
                        ? action.payload
                        : data
                ),
                actualData: {}
            }

        case types.MsgError:

            return {
                ...state,
                msgError: action.payload
            }

        case types.RemoveMsgError:

            return {
                ...state,
                msgError: null
            }
        
        // case types.SearchId:
        //     state.dataList.find()
        // const found = dataList.find(elem => elem.id === pokemon.id);


        default:
            return state
    }
}