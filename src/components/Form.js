import React, { useContext, useEffect } from 'react'
import { AddPokemon, RemoveMsgError, SetMsgError, UpdatePokemon } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext'
import { useForm } from '../hooks/useForm'
import { MsgError } from './MsgError';
import { pokemonExist } from '../helpers/alerts';

import { useExistActualData } from '../hooks/useActualData'

export const Form = () => {

    const { state, dispatch} = useContext(AuthContext);
    const { actualData, msgError } = state;
    // console.log(actualData)
    // console.log(state);

   


    const initialState = {
        idPokemon: '',
        name: '',
        type: '',
        img: '',
    }

    const [formValues, handleInputChange, reset, setForm] = useForm(initialState);

    // if actualData trae datos cambia el form
    useEffect(() => {
      if(actualData.idPokemon){
          setForm(actualData);
      } else {
          setForm(initialState);
      }
    }, [actualData])
    

    const { idPokemon, name, type , img } = formValues;

    const handleSumit = ( e ) => {
        e.preventDefault();
        // console.log(formValues)
        // condicion si actualiza o guarda
        if( actualData.idPokemon ){
            // actuliza
            if(isFormValid()){
                dispatch( UpdatePokemon(formValues));
                reset();
            }

        } else {
            if(isFormValid()){
                // add 
                dispatch( AddPokemon(formValues));
                reset();
            }
        }

    } 

    // validacion del Form
    const isFormValid = () => {

        const msg = `of Pokemon is required`;

        if (idPokemon.trim().length === 0 ){
            dispatch(SetMsgError(`Id ${msg}`));
            return false;
        } else if (name.trim().length === 0 ){
            dispatch(SetMsgError(`Name ${msg}`));
            return false;
        } else if (type.trim().length === 0 ){
            dispatch( SetMsgError(`The Type ${msg}`));
            return false;
        } else if ( img.trim().length === 0 ){
            dispatch( SetMsgError(`The URL ${msg}`))
            return false;
        }

        // validar id
        if( pokemonExist( state.dataList, {id: idPokemon}) ){
            dispatch(SetMsgError(`Id has been added`));
            reset();
            return false
        }

        dispatch(RemoveMsgError());
        return true;
    }

    // disabled: dataActual Existe
    // const [ exist ] = useExistActualData(actualData);
    
    // const exitsActualData = () => {
    //     ( Object.keys(actualData).length === 0)
    //         ? false
    //         : true
    // }
    

  return (
    <div>
        <h3 className='text-center text-primary'>Register Pokemon</h3>
        <hr className='text-primary'/>
        {
            msgError && <MsgError />
        }

        <form
            onSubmit={ handleSumit }
        >
            <div className="mb-3">
                <label className="form-label">Id Pokemon:</label>
                <input 
                    type="text" 
                    className="form-control" 
                    name='idPokemon'
                    autoFocus='on'
                    value={ idPokemon }
                    onChange={ handleInputChange }
                    disabled={ (Object.keys(actualData).length === 0) ? false : true}
                    // disabled={ exist }
                />
            </div>
            <div className="mb-3">
                <label className="form-label">Name Pokemon:</label>
                <input 
                    type="text" 
                    className="form-control" 
                    name='name'
                    // autoFocus='on'
                    value={ name }
                    onChange={ handleInputChange }
                />
            </div>
            <div className="mb-3">
                <label className="form-label">Type Pokemon:</label>
                <input 
                    type="text" 
                    className="form-control" 
                    name='type'
                    // autoFocus='on'
                    value={ type }
                    onChange={ handleInputChange }
                />
            </div>
            <div className="mb-3">
                <label className="form-label">Url Pokemon:</label>
                <input 
                    type="text" 
                    className="form-control" 
                    name='img'
                    // autoFocus='on'
                    value={ img }
                    onChange={ handleInputChange }
                    // disabled = { exist }
                    disabled={ (Object.keys(actualData).length === 0) ? false : true}

                />
            </div>
            <input 
                type="submit" 
                className={ (actualData.idPokemon) 
                            ? 'btn btn-outline-warning w-100' 
                            : 'btn btn-outline-primary w-100' 
                            }
                value={ (actualData.idPokemon)
                        ? 'Update Pokemon'
                        : 'Add Pokemon'    
                        }
            />
        </form>
        
    </div>
  )
}
