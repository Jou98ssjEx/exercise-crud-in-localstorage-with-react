export const routes = [
    {
        name: 'Home',
        route: '/',
    },
    {
        name: 'Pokemons',
        route: '/pokemons',
    },
    {
        name: 'Search',
        route: '/search',
    },
]