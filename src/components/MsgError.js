import React, { useContext } from 'react'
import { AuthContext } from '../auth/AuthContext'

export const MsgError = () => {
  const {state} = useContext(AuthContext);
  const { msgError } = state;
  return (
    <div className="alert alert-danger text-center p-2" role="alert">
        {msgError}
    </div>
  )
}
