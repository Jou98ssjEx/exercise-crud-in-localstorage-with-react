import React, { useContext, useEffect } from 'react'
import { RemoveMsgError, SetMsgError } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext';
import { usePokemon } from '../hooks/usePokemons'
import { AddPokemons } from './AddPokemons';
import { Loading } from './Loading';
import { MsgError } from './MsgError';

export const ShowPokemon = ({ pokemonIdOrName = '' }) => {
  
    
    const { dispatch } = useContext(AuthContext);

    const { pokemon, loading, msgError } = usePokemon(pokemonIdOrName);
    useEffect(() => {
      
        // console.log(Object.keys(pokemon).length === 0)
        validateResponseOfPokemon();
    }, [msgError])
    
    // console.log(pokemon)
    const validateResponseOfPokemon = ( ) => {
      if(msgError){
        dispatch(SetMsgError(`${pokemonIdOrName} don't valid`))
        return false;
      }
      dispatch(RemoveMsgError());
      return true;
    }

  return (
    <div className='col-12 col-md-6 col-6'>
        {
            (loading)
                ? <Loading />
                // :   (
                //     <AddPokemons 
                //         key={ pokemon.id }
                //        pokemon= { pokemon }
                //     />
                // )
                : (msgError)
                  ? <MsgError />
                    :   (
                        <AddPokemons 
                            key={ pokemon.id }
                          pokemon= { pokemon }
                        />
                    )
        }
        
    </div>
  )
}
