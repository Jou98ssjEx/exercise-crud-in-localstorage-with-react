import React from 'react'

export const Loading = () => {
  return (
    <div>
        <div className="d-flex justify-content-center">
            <div className="spinner-grow text-info" role="status">
                <span className="sr-only">Loading...</span>
            </div>
            {/* <div className="spinner-border" role="status">
            </div> */}
        </div>
    </div>
  )
}
