import React from 'react'
import { Form } from '../components/Form'
import { Table } from '../components/Table'

export const HomePage = () => {
  return (
    <div className=' container'>
      <h2 className='text-success text-center'>HomePage</h2>
      <hr  className='text-success'/>

      <div className='row'>
        <div className='col-12 col-md-5'>
          <div className='card p-4'>
              <Form />
          </div>

        </div>
        <div className='col-12 col-md-7'>
          <div className='mt-1'>
            <Table />
          </div>
        </div>

      </div>
    </div>
  )
}
