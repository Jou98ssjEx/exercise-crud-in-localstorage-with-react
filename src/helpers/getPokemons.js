import { getPokemonApi, getPokemonApiById } from "../api/pokemonApi"

const limit = '' ;
// const limit = '?limit=850' ;

export const getPokemonsOfApi = async ( ) => {

    const { data } = await getPokemonApi.get(`/pokemon${limit}`);
    // console.log(data)
    const { results } = data;

    return await transformDataPokemon(results);
}

export const getPokemon = async ( id = '1' ) => {
    const { data } = await getPokemonApiById.get(`${id}`);

    // Condicion para que no apareceza error cuando cargue la primera vez
    if (data.previous === null ){
        return {}
    }

    const typeofPokemon = data.types[0].type.name;
    // const typeofPokemon = data.types[0].type.name.replace(/^\w/, (c) => c.toUpperCase());
    const ability = data.abilities[0].ability.name.replace(/^\w/, (c) => c.toUpperCase());
    const experence = data.base_experience;
    const img = data.sprites.front_default;
    const name = data.species.name.replace(/^\w/, (c) => c.toUpperCase());
    const body = `Pokemon type of ${typeofPokemon}`;
    // console.log(typeofPokemon);
    return {
        id,
        ability,
        experence,
        name,
        img,
        body,
        typeofPokemon,
    }
}

const transformDataPokemon = async ( results = [] ) =>{
    
    let dataPokemon = [];

    for (let i = 0; i < results.length; i++) {

        const { name, url } = results[i];
        const arrPokemon = url.split('/');
        const id = arrPokemon[6];
        const img = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`

        // extrae lo necesario del pokemon
        const {typeofPokemon, ability, experence,body} = await getPokemon(id);

        // inserta pokemon al arreglo
        dataPokemon.push(
            {
                id,
                name: name.replace(/^\w/, (c) => c.toUpperCase()),
                img,
                type: typeofPokemon,
                ability,
                experence,
                body
            }
        )

        
    }

    return dataPokemon;
} 