import Swal from "sweetalert2";


export const pokemonExist = ( pokeArr, pokemon ) => {

    const found = pokeArr.find(elem => elem.id === pokemon.id);
    const exist = (found !== undefined) ? true : false;

    if(exist){
        Swal.fire({
            title: 'Pokemon has been added',
            timer:1500,
            showConfirmButton: false,
          })
    }

    return exist;

}