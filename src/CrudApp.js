import React, { useEffect, useReducer } from 'react'
import { AuthContext } from './auth/AuthContext'
import { Footer } from './components/Footer'
import { crudReducer } from './reducers/crudReducer'
import { AppRouters } from './routes/AppRouters'

export const CrudApp = () => {

  // init 
  const init = ( ) => {
    return JSON.parse( localStorage.getItem('pokemons')) || {
      auth: {
          user: 'Barry Allen',
          email: 'flash98@labstart.com',
      },
      dataList: [],
      actualData: {},
      msgError: null,
    }
  }

  // reducer 
  const [state, dispatch] = useReducer(crudReducer, {}, init)

  // cargar el state

  useEffect(() => {
    localStorage.setItem('pokemons', JSON.stringify(state));
  }, [state])
  

  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch
      }}
    > 
      <AppRouters/>

      <Footer />

    </AuthContext.Provider>
  )
}
