import React, { useContext } from 'react'
import { AddPokemon } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext';
import { pokemonExist } from '../helpers/alerts';

export const AddPokemons = ({pokemon}) => {
    // console.log({pokemon})
    const { name, experence, move, img, body, ability, typeofPokemon } = pokemon;
    const { dispatch, state } = useContext(AuthContext);

    const handleAddPokemon = () =>{
        const { dataList } = state;
        if (!pokemonExist(dataList, pokemon)){
        
            dispatch(AddPokemon({
                ...pokemon,
                idPokemon: pokemon.id,
                type: pokemon.typeofPokemon
            }))
        }
    }

  return (
    <div 
        className={`${'card p-4'}  pointer ${typeofPokemon}`}
        onClick={ handleAddPokemon }
    >
        <div className="row g-0">
            <div className={`col-md-4 `}>
                <img 
                    // style={ { 'height': '150px', 'width': '200px', 'textAlign': 'center'}}
                    src={ img} 
                    className={`card-img`} 
                    // className={`img-fluid`} 
                    alt="..."
                />
            </div>
            <div className="col-md-8">
                <div className="card-body">
                    <h5 className="card-title text-center"> { name} </h5>
                </div>
                <div className='d-flex flex-column'>
                    <label> <b>Exp:</b> {  experence } </label>
                    <label><b>Abi:</b> {  ability } </label>
                    <label> <b>Mov:</b> {  move } </label>
                </div>
                <div className='card-footer text-center'>
                    {  body }
                </div>
            </div>
        </div>
    </div>
  )
}
