export const types = {
    'Add': 'Add',
    'Update': 'Update',
    'Actual': 'Actual',
    'Delete': 'Delete',
    'Read': 'Read',
    'MsgError': 'MsgError',
    'RemoveMsgError': 'RemoveMsgError',
    'SearchId': 'SearchId',
}