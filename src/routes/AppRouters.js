import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Navbar } from '../components/Navbar'
import { HomePage } from '../pages/HomePage'
import { PokemonsPage } from '../pages/PokemonsPage'
import { SearchPage } from '../pages/SearchPage'

export const AppRouters = () => {
  
  return (
    <BrowserRouter>
        <Navbar />
        <Routes>
            <Route path='/' element={ <HomePage/> } />
            <Route path='/pokemons' element={ <PokemonsPage /> } />
            <Route path='/search' element={ <SearchPage /> } />
        </Routes>
    </BrowserRouter>
  )
}
