import React, { useContext } from 'react'
import { AuthContext } from '../auth/AuthContext'
import { DataListPokemons } from './DataListPokemons';

export const Table = () => {

  const {state} = useContext(AuthContext);
  const { dataList } = state;
  return (
    <div>
        <h3 className='text-center text-info'>Pokemons List</h3>
        <hr className='text-info'/>
        <div className='table-responsive-lg table__fix'>
          <table className="table table-striped table-hover">
          <thead className='text-center fix__header_t'>
              <tr>
              <th scope="col">Actions</th>
              <th scope="col">Id</th>
              <th scope="col">Name</th>
              <th scope="col">Img</th>
              <th scope="col">Type</th>
              </tr>
          </thead>
          <tbody>

            {
              dataList.map( (m, i) => (
                <DataListPokemons 
                  key={ i }
                  { ...m }
                    dataPokemon = { m }
                /> 
              ))
            }
            
          </tbody>
          </table>

        </div>
    </div>
  )
}
