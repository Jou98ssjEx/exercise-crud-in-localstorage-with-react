import React, { useContext } from 'react'
import { ActualPokemon, DeletePokemon } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext'

export const DataListPokemons = ( { idPokemon, name, type,  img, dataPokemon }) => {
    
    const { dispatch } = useContext(AuthContext);

    const handleDeletePokemon = (  ) => {
        // e.preventDefault();
        dispatch(DeletePokemon( idPokemon));
    }

    const handleActualPokemon =  ( ) =>{
        dispatch(ActualPokemon( dataPokemon ) );
    }
  return (
    <tr className='align-middle text-center'>
        <th scope="row">
            <div className='d-flex justify-content-between'>
                <button
                    className='btn btn-outline-danger'
                    onClick={ handleDeletePokemon }
                >
                    <i className='bx bxs-message-square-x'></i>
                </button>
                <button
                    className='btn btn-outline-info'
                    onClick={ handleActualPokemon }
                >
                    <i className='bx bxs-edit'></i>
                </button>

            </div>
        </th>
        <td> {idPokemon} </td>
        <td> {name} </td>
        <td> 
            <img src={img} alt={name} />
        </td>
        <td> {type.replace(/^\w/, (c) => c.toUpperCase()) } </td>
    </tr>
  )
}
