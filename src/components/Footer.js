import React from 'react'

export const Footer = () => {
    const anio = new Date().getFullYear();
  return (
    <footer className='p-2'>
        <div className=''>
            &copy; { anio } Copyrigth : JouG
        </div>
    </footer>
  )
}
