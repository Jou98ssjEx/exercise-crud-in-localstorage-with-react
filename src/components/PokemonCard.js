import React, { useContext } from 'react'
import Swal from 'sweetalert2';
import { AddPokemon } from '../actions/crudActions';
import { AuthContext } from '../auth/AuthContext';
import { pokemonExist } from '../helpers/alerts';

export const PokemonCard = ( { pokemon } ) => {
  const {type, name, img} =  pokemon;
  const {dispatch, state} = useContext(AuthContext);

  // console.log(state.dataList)
  const handleClickPoke = () =>{
    const { dataList } = state;
    if (!pokemonExist(dataList, pokemon)){
      dispatch(AddPokemon({
          ...pokemon,
          idPokemon: pokemon.id,
          type
        }))
    }
  }
  
  return (
    <div className='pokeCard col-12 col-sm-4 col-md-3 col-lg-2 '>
        <div 
            className={`card mb-3 pointer ${type}`} 
            onClick={ handleClickPoke }
        >
            <img src={img} className="card-img-top" alt={name}/>
            <div className="card-body ">
                <h6 className="card-title text-center "> {name} </h6>
            </div>
           
        </div>
    </div>
  )
}
