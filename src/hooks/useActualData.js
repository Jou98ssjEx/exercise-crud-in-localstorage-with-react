import { useState } from "react";

export const useExistActualData = ( actualData ) => {
    
    const [exist, setExist] = useState(false);

    // const handleActualData = () =>{
        if (Object.keys(actualData).length ===0){
            setExist(false);
        } else {
            setExist(true);
        }
    // }

    // return [ exist, handleActualData ];
    // ( Object.keys(actualData).length === 0)
    // ? false
    // : true

    return [exist];
}